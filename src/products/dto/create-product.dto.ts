import { IsNotEmpty, IsPositive } from 'class-validator';

export class CreateProductDto {
  id: number;
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @IsPositive()
  price: number;
}
